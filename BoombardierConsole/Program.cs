﻿using BoombardierEngine;
using System;

namespace BoombardierConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var w = Console.BufferWidth;
            var h = Console.BufferHeight;

            Console.WriteLine(w + "/" + h);
            Console.WriteLine("wt:" + Console.WindowTop + "/wh:" + Console.WindowHeight + "/wl:" + Console.WindowLeft + "/ww:" + Console.WindowWidth);

            Board board = new Board(Console.WindowWidth, Console.WindowHeight);
            board.Initialize();
            GameEngine ge = new GameEngine(board);
            using (GameDraftsman gameDraftsman = new GameDraftsman(ge))
            {
                while (true)
                {
                    ge.Calculate();


                    gameDraftsman.ClearBuffer();
                    gameDraftsman.DrawToBuffer();
                    gameDraftsman.DrawToScreen();


                    if (Console.KeyAvailable)
                    {
                        break;
                    }
                }
            }
        }
    }
}
