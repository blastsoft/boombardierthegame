﻿using BoombardierEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BoombardierConsole
{
    public class GameDraftsman : IDisposable
    {
        private GameEngine ge;
        private int width { get { return ge.Board.Width; } }
        private int height { get { return ge.Board.Height; } }

        public char[] backBuffer = null;

        readonly BufferedStream screenBuffer;

        public GameDraftsman(GameEngine ge)
        {
            this.ge = ge;

            Console.OutputEncoding = Encoding.Unicode;

            screenBuffer = new BufferedStream(Console.OpenStandardOutput(), width * height);

            backBuffer = new char[width * height];
        }

        public void DrawToScreen()
        {
            Console.CursorVisible = false;
            Console.SetCursorPosition(0, 0);

            var buffer = new byte[backBuffer.Length << 1];
            Encoding.Unicode.GetBytes(backBuffer, 0, backBuffer.Length, buffer, 0);

            lock (screenBuffer)
            {
                screenBuffer.Write(buffer, 0, buffer.Length);
            }
        }

        public void ClearBuffer()
        {
            for (int i = 0; i < backBuffer.Length; i++)
            {
                backBuffer[i] = ' ';
            }
        }

        public void DrawToBuffer()
        {
            foreach (BoardElement boardElement in ge.Board.BoardElements)
            {
                if (boardElement == null)
                    continue;

                if (boardElement is Building)
                {
                    Building building = (Building)boardElement;
                    for (int y = 0; y < building.Height; y++)
                    {
                        PutCharacter('#', building.X, height - 1 - y);
                    }
                }
            }

            DrawText("FPS: " + ge.FPS.ToString(), 0, 0);
        }

        private void PutCharacter(char character, int x, int y)
        {
            backBuffer[y * width + x] = character;
        }

        private void DrawText(string text, int x, int y)
        {
            int len = text.Length;
            for (int i = x; i < len; i++)
            {
                if (i >= width)
                    continue;
                char c = text[i - x];
                backBuffer[y * width + x + i] = c;
            }
        }

        public void Dispose()
        {
            if (screenBuffer != null)
            {
                screenBuffer.Dispose();
            }
        }
    }
}
