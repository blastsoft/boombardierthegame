﻿using System;
using System.Collections.Generic;

namespace BoombardierEngine
{
    public class Board
    {
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public List<BoardElement> BoardElements { get; private set; } 

        public Board(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public void Initialize()
        {
            BoardElements = new List<BoardElement>();   
            Random rnd = new Random();
            for (int i = 0; i < Width; i++)
            {
                if (i % 2 == 0) continue;
                Building building = new Building(i, rnd.Next(1, Height / 2));
                BoardElements.Add(building);
            }
        }
    }
}
