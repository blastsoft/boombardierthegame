﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoombardierEngine
{
    public class Boomb : BoardElement
    {
        
        public override void ContactWith(BoardElement otherElement)
        {
            if (otherElement is Building)
            {
                //((Building)otherElement).Height--;
            }
        }

        public override void Destroy()
        {
            this.IsAlive = false;
        }
    }
}
