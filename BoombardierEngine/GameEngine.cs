﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoombardierEngine
{
    public class GameEngine
    {
        private DateTime? previousTick = null;

        public int FPS { get; set; } = 0;

        public Board Board { get; private set; }

        public GameEngine(Board board)
        {
            this.Board = board;
        }

        public void Calculate()
        {
            TimeSpan ticks = new TimeSpan(0L);
            DateTime now = DateTime.Now;
            
            if (previousTick != null)
            {
                ticks = now - previousTick.Value;
                FPS = (int)(10000000 / ticks.Ticks);
            }
            previousTick = now;
        }
    }
}
