﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoombardierEngine
{
    public class Building : BoardElement
    {
        public int Height { get; private set; }

        public Building(int location, int height)
        {
            this.X = location;
            this.Height = height;
        }

        public void DestroyOneFloor()
        {
            Height--;
            if(Height == 0)
            {
                this.Destroy();
            }
        }

        public override void ContactWith(BoardElement otherElement)
        {
            
        }
    }
}
