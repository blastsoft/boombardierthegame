﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoombardierEngine
{
    public class BoardElement
    {
        public int X { get; set; }
        public int Y { get; set; }

        public bool IsAlive { get; set; } = true;

        public virtual void InteractWith(BoardElement otherElement)
        {
            if (otherElement == null)
                return;
            if (otherElement.X == X && otherElement.Y == Y)
            {
                this.ContactWith(otherElement);
            }
        }

        public virtual void ContactWith(BoardElement otherElement)
        {

        }

        public virtual void Destroy()
        {
            this.IsAlive = false;
        }
    }
}
